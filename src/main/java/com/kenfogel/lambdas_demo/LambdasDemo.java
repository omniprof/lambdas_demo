package com.kenfogel.lambdas_demo;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * LambdasDemo
 *
 * @author cdea
 */
public class LambdasDemo {

    private void assignVariablesToFunctionalInterfaces() {
        System.out.println("\nassignVariablesToFunctionalInterfaces");
        // assigning variables to functional interfaces
        MyFormula area = (height, width) -> height * width;
        MyFormula perimeter = (height, width) -> 2 * height + 2 * width;

        System.out.println("Area = " + area.compute(3, 4));
        System.out.println("Perimeter = " + perimeter.compute(3, 4));
    }

    private void usePredicateConsumer() {
        System.out.println("\nuseMultipleInterfaces");
        // create a list of values
        List<Integer> values = Arrays.asList(23, 84, 74, 85, 54, 60);
        System.out.println("values: " + values.toString());

        // non-local variable to be used in lambda expression.
        int threshold = 54;
        System.out.println("Values greater than " + threshold + " converted to hex:");
//        Stream<Integer> stream = values.stream();//
        // using aggregate functions filter() and forEach()
//        stream
        values.stream()
                .filter(val -> val > threshold) // Predicate interface
                .sorted()
                .map(dec -> Integer.toHexString(dec)) // Function interface
                .forEach(val -> System.out.println(val)); // for each output values.

    }

    public void perform() {
        assignVariablesToFunctionalInterfaces();
        usePredicateConsumer();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LambdasDemo ld = new LambdasDemo();
        ld.perform();
        System.exit(0);
    }

}
