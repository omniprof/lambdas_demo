package com.kenfogel.lambdas_demo;

@FunctionalInterface
public interface MyFormula {

    double compute(double val1, double val2);
}

/*
// Could also be declared in a class
public class FunctioanlInterfaces {
    @FunctionalInterface
    public interface MyFormula {

        double compute(double val1, double val2);
    }
}
*/
